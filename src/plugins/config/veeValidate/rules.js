import { REGEXP_PHONE, REGEXP_EMAIL, REGEXP_PASSWORD, REGEXP_PHONE_CODE } from 'src/const/regexp';
import { MASK_PHONE } from 'const/mask';
import dayjs from 'dayjs';

export const phoneOrEmailRule = {
    validate(value, args) {
        // Custom regex for a phone number
        let phone = REGEXP_PHONE.test(value);
        let email = REGEXP_EMAIL.test(value);

        let valid = false;
        let key = 'phone';

        if (phone) {
            const cleanPhone = Array.from(value).reduce((acc, val, index) => {
                if (val !== MASK_PHONE.ru[index] && value) acc += val;
                return acc;
            }, '');
            valid = cleanPhone.length === 10;
        } else {
            if (email) valid = true;
            key = 'email';
        }

        return new Promise(resolve => {
            resolve({
                valid,
                data: {
                    key
                }
            });
        });
    }
};

export const email = {
    validate(value, args) {
        return new Promise(resolve => {
            resolve({
                valid: REGEXP_EMAIL.test(value)
            });
        });
    }
};

export const password = {
    validate(value, args) {
        const valid = REGEXP_PASSWORD.test(value) && value.length >= 6;

        return new Promise(resolve => {
            resolve({
                valid
            });
        });
    }
};

export const repassword = {
    validate(repassword, [password]) {
        return new Promise(resolve => {
            resolve({
                valid: repassword === password
            });
        });
    }
};

export const phoneCode = {
    validate(value, args) {
        return new Promise(resolve => {
            resolve({
                valid: REGEXP_PHONE_CODE.test(value)
            });
        });
    }
};

export const seriesPassport = {
    validate(value) {
        return new Promise(resolve => {
            resolve({
                valid: /^[0-9]{4}?[-]?[0-9]{6}$/g.test(value)
                // valid: /^[0-9 ]{5}?[0-9]{6}$/g.test(value)
            });
        });
    }
};

export const datePassport = {
    validate(value) {
        const isValid = (cDate) => {
            cDate = cDate.split('.');
            let current = dayjs(`${cDate[1]}.${cDate[0]}.${cDate[2]}`);
            let minDate = dayjs(dayjs('10.01.1997').format('YYYY.MM.DD'));
            let maxDate = dayjs(new Date());

            return !!((current >= minDate && current <= maxDate));
        };

        return new Promise(resolve => {
            resolve({
                valid: isValid(value)
            });
        });
    }
};

export const codePassport = {
    validate(value) {
        return new Promise(resolve => {
            resolve({
                valid: /^[0-9]{3}?[-]?[0-9]{3}$/g.test(value)
            });
        });
    }
};

export const bDay = {
    validate(value) {
        const isValid = (date) => {
            date = date.split('.');
            let bDay = dayjs(`${date[1]}.${date[0]}.${date[2]}`);
            let minBDay = dayjs(new Date()).subtract(18, 'year');
            let maxBDay = dayjs(new Date()).subtract(100, 'year');

            return !!((bDay > maxBDay && bDay < minBDay));
        };

        return new Promise(resolve => {
            resolve({
                valid: isValid(value)
            });
        });
    }
};

export const requireHideName = {
    validate(value) {
        console.warn(value);

        return new Promise(resolve => {
            resolve({
                valid: value.trim() !== ''
            });
        });
    }
};
