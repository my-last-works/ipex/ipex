import Vue from 'vue';
const requireComponent = require.context(
    'src/templates/',
    false,
    /Template[A-Z]\w+\.(vue|js)$/
);

requireComponent.keys().forEach(fileName => {
    const componentConfig = requireComponent(fileName);
    const componentName = fileName.split('/')
        .pop()
        .replace(/\.\w+$/, '')
        .replace(/([a-z0-9]|(?=[A-Z]))([A-Z])/g, '$1-$2')
        .toLowerCase()
        .slice(1);

    Vue.component(
        componentName,
        componentConfig.default || componentConfig
    );
});
